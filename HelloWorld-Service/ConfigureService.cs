﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace HelloWorld_Service
{
    internal static class ConfigureService
    {
        internal static void Configure()
        {
            HostFactory.Run(configure =>
            {
                configure.Service<HelloWorldService>(service =>
                {
                    service.ConstructUsing(s => new HelloWorldService());
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });
                //Setup Account that window service use to run.  
                configure.RunAsLocalSystem();
                configure.SetServiceName("HelloWorldService");
                configure.SetDisplayName("HelloWorldService");
                configure.SetDescription("Jared's Hello World service for testing Azure DevOps as a software deployment tool.");
            });
        }
    }
}
